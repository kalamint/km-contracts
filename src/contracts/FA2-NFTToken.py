##
## ## Introduction
##
## See the FA2 standard definition:
## <https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/>
##
## See more examples/documentation at
## <https://gitlab.com/smondet/fa2-smartpy/> and
## <https://assets.tqtezos.com/docs/token-contracts/fa2/1-fa2-smartpy/>.
##
import smartpy as sp
##
## ## Meta-Programming Configuration
##
## The `FA2_config` class holds the meta-programming configuration.
##
class FA2_config:
    def __init__(self,
                 debug_mode                   = False,
                 single_asset                 = False,
                 non_fungible                 = True,
                 add_mutez_transfer           = False,
                 readable                     = True,
                 force_layouts                = True,
                 support_operator             = True,
                 assume_consecutive_token_ids = True,
                 add_permissions_descriptor   = False,
                 lazy_entry_points = False,
                 lazy_entry_points_multiple = False
                 ):

        if debug_mode:
            self.my_map = sp.map
        else:
            self.my_map = sp.big_map
        # The option `debug_mode` makes the code generation use
        # regular maps instead of big-maps, hence it makes inspection
        # of the state of the contract easier.

        self.single_asset = single_asset
        # This makes the contract save some gas and storage by
        # working only for the token-id `0`.

        self.non_fungible = non_fungible
        # Enforce the non-fungibility of the tokens, i.e. the fact
        # that total supply has to be 1.

        self.readable = readable
        # The `readable` option is a legacy setting that we keep around
        # only for benchmarking purposes.
        #
        # User-accounts are kept in a big-map:
        # `(user-address * token-id) -> ownership-info`.
        #
        # For the Babylon protocol, one had to use `readable = False`
        # in order to use `PACK` on the keys of the big-map.

        self.force_layouts = force_layouts
        # The specification requires all interface-fronting records
        # and variants to be *right-combs;* we keep
        # this parameter to be able to compare performance & code-size.

        self.support_operator = support_operator
        # The operator entry-points always have to be there, but there is
        # definitely a use-case for having them completely empty (saving
        # storage and gas when `support_operator` is `False).

        self.assume_consecutive_token_ids = assume_consecutive_token_ids
        # For a previous version of the TZIP specification, it was
        # necessary to keep track of the set of all tokens in the contract.
        #
        # The set of tokens is for now still available; this parameter
        # guides how to implement it:
        # If `true` we don't need a real set of token ids, just to know how
        # many there are.

        self.add_mutez_transfer = add_mutez_transfer
        # Add an entry point for the administrator to transfer tez potentially
        # in the contract's balance.

        self.add_permissions_descriptor = add_permissions_descriptor
        # Add the `permissions_descriptor` entry-point; it is an
        # optional part of the specification and
        # costs gas and storage so we keep the option of not adding it.
        #
        # Note that if `support_operator` is `False`, the
        # permissions-descriptor becomes technically required.

        self.lazy_entry_points = lazy_entry_points
        self.lazy_entry_points_multiple = lazy_entry_points_multiple
        #
        # Those are “compilation” options of SmartPy into Michelson.
        #
        if lazy_entry_points and lazy_entry_points_multiple:
            raise Exception(
                "Cannot provide lazy_entry_points and lazy_entry_points_multiple")

        name = "FA2"
        if debug_mode:
            name += "-debug"
        if single_asset:
            name += "-single_asset"
        if non_fungible:
            name += "-nft"
        if add_mutez_transfer:
            name += "-mutez"
        if not readable:
            name += "-no_readable"
        if not force_layouts:
            name += "-no_layout"
        if not support_operator:
            name += "-no_ops"
        if not assume_consecutive_token_ids:
            name += "-no_toknat"
        if add_permissions_descriptor:
            name += "-perm_desc"
        if lazy_entry_points:
            name += "-lep"
        if lazy_entry_points_multiple:
            name += "-lepm"
        self.name = name

## ## Auxiliary Classes and Values
##
## The definitions below implement SmartML-types and functions for various
## important types.
##
token_id_type = sp.TNat

class Error_message:
    def __init__(self, config):
        self.config = config
        self.prefix = "FA2_"
    def make(self, s): return (self.prefix + s)
    def token_undefined(self):       return self.make("TOKEN_UNDEFINED")
    def insufficient_balance(self):  return self.make("INSUFFICIENT_BALANCE")
    def not_operator(self):          return self.make("NOT_OPERATOR")
    def not_owner(self):             return self.make("NOT_OWNER")
    def operators_unsupported(self): return self.make("OPERATORS_UNSUPPORTED")

## The current type for a batched transfer in the specification is as
## follows:
##
## ```ocaml
## type transfer = {
##   from_ : address;
##   txs: {
##     to_ : address;
##     token_id : token_id;
##     amount : nat;
##   } list
## } list
## ```
##
## This class provides helpers to create and force the type of such elements.
## It uses the `FA2_config` to decide whether to set the right-comb layouts
class Batch_transfer:
    def __init__(self, config):
        self.config = config
    def get_transfer_type(self):
        tx_type = sp.TRecord(to_ = sp.TAddress,
                             token_id = token_id_type,
                             amount = sp.TNat)
        if self.config.force_layouts:
            tx_type = tx_type.layout(
                ("to_", ("token_id", "amount"))
            )
        transfer_type = sp.TRecord(from_ = sp.TAddress,
                                   txs = sp.TList(tx_type)).layout(
                                       ("from_", "txs"))
        return transfer_type
    def get_type(self):
        return sp.TList(self.get_transfer_type())
    def item(self, from_, txs):
        v = sp.record(from_ = from_, txs = txs)
        return sp.set_type_expr(v, self.get_transfer_type())
##
## `Operator_param` defines type types for the `%update_operators` entry-point.
class Operator_param:
    def __init__(self, config):
        self.config = config
    def get_type(self):
        t = sp.TRecord(
            owner = sp.TAddress,
            operator = sp.TAddress,
            token_id = token_id_type)
        if self.config.force_layouts:
            t = t.layout(("owner", ("operator", "token_id")))
        return t
    def make(self, owner, operator, token_id):
        r = sp.record(owner = owner,
                      operator = operator,
                      token_id = token_id)
        return sp.set_type_expr(r, self.get_type())

## The class `Ledger_key` defines the key type for the main ledger (big-)map:
##
## - In *“Babylon mode”* we also have to call `sp.pack`.
## - In *“single-asset mode”* we can just use the user's address.
class Ledger_key:
    def __init__(self, config):
        self.config = config
    def make(self, user, token):
        user = sp.set_type_expr(user, sp.TAddress)
        token = sp.set_type_expr(token, token_id_type)
        if self.config.single_asset:
            result = user
        else:
            result = sp.pair(user, token)
        if self.config.readable:
            return result
        else:
            return sp.pack(result)

## For now a value in the ledger is just the user's balance. Previous
## versions of the specification required more information; potential
## extensions may require other fields.
class Ledger_value:
    def get_type():
        return sp.TRecord(balance = sp.TNat)
    def make(balance):
        return sp.record(balance = balance)

## The link between operators and the addresses they operate is kept
## in a *lazy set* of `(owner × operator × token-id)` values.
##
## A lazy set is a big-map whose keys are the elements of the set and
## values are all `Unit`.
class Operator_set:
    def __init__(self, config):
        self.config = config
    def inner_type(self):
        return sp.TRecord(owner = sp.TAddress,
                          operator = sp.TAddress,
                          token_id = token_id_type
                          ).layout(("owner", ("operator", "token_id")))
    def key_type(self):
        if self.config.readable:
            return self.inner_type()
        else:
            return sp.TBytes
    def make(self):
        return self.config.my_map(tkey = self.key_type(), tvalue = sp.TUnit)
    def make_key(self, owner, operator, token_id):
        metakey = sp.record(owner = owner,
                            operator = operator,
                            token_id = token_id)
        metakey = sp.set_type_expr(metakey, self.inner_type())
        if self.config.readable:
            return metakey
        else:
            return sp.pack(metakey)
    def add(self, set, owner, operator, token_id):
        set[self.make_key(owner, operator, token_id)] = sp.unit
    def remove(self, set, owner, operator, token_id):
        del set[self.make_key(owner, operator, token_id)]
    def is_member(self, set, owner, operator, token_id):
        return set.contains(self.make_key(owner, operator, token_id))

class Balance_of:
    def request_type():
        return sp.TRecord(
            owner = sp.TAddress,
            token_id = token_id_type).layout(("owner", "token_id"))
    def response_type():
        return sp.TList(
            sp.TRecord(
                request = Balance_of.request_type(),
                balance = sp.TNat).layout(("request", "balance")))
    def entry_point_type():
        return sp.TRecord(
            callback = sp.TContract(Balance_of.response_type()),
            requests = sp.TList(Balance_of.request_type())
        ).layout(("requests", "callback"))

# below is used everywhere in the code, tzip-16 metadata uri is actually in token_metadata big map
class Token_meta_data:
    def __init__(self, config):
        self.config = config
    def get_type(self):
        t = sp.TRecord(
            decimals = sp.TNat,
            extras = sp.TMap(sp.TString, sp.TString),
            token_id = token_id_type,
            price = sp.TMutez,
            ipfs_hash = sp.TString,
            owner = sp.TAddress,
            on_sale = sp.TBool,
            on_auction = sp.TBool,
            collection_id = sp.TNat,
            creator = sp.TAddress,
            creator_royalty = sp.TNat, 
            edition_number = sp.TNat,
            editions = sp.TNat
            
        )
        if self.config.force_layouts:
            t = t.layout( ("decimals",
                          ("extras",
                           ("token_id",
                            ("price",
                             ("ipfs_hash",
                              ("owner",
                               ("on_sale",
                               ("on_auction",
                               ("collection_id",
                               ("creator",
                               ("creator_royalty",
                                ("edition_number","editions")))))))))))))
                                
        return t
    def set_type_and_layout(self, expr):
        sp.set_type(expr, self.get_type())
    def request_type(self):
        return token_id_type

class Permissions_descriptor:
    def __init__(self, config):
        self.config = config
    def get_type(self):
        operator_transfer_policy = sp.TVariant(
            no_transfer = sp.TUnit,
            owner_transfer = sp.TUnit,
            owner_or_operator_transfer = sp.TUnit)
        if self.config.force_layouts:
            operator_transfer_policy = operator_transfer_policy.layout(
                                       ("no_transfer",
                                        ("owner_transfer",
                                         "owner_or_operator_transfer")))
        owner_transfer_policy =  sp.TVariant(
            owner_no_hook = sp.TUnit,
            optional_owner_hook = sp.TUnit,
            required_owner_hook = sp.TUnit)
        if self.config.force_layouts:
            owner_transfer_policy = owner_transfer_policy.layout(
                                       ("owner_no_hook",
                                        ("optional_owner_hook",
                                         "required_owner_hook")))
        custom_permission_policy = sp.TRecord(
            tag = sp.TString,
            config_api = sp.TOption(sp.TAddress))
        main = sp.TRecord(
            operator = operator_transfer_policy,
            receiver = owner_transfer_policy,
            sender   = owner_transfer_policy,
            custom   = sp.TOption(custom_permission_policy))
        if self.config.force_layouts:
            main = main.layout(("operator",
                                ("receiver",
                                 ("sender", "custom"))))
        return main
    def set_type_and_layout(self, expr):
        sp.set_type(expr, self.get_type())
    def make(self):
        def uv(s):
            return sp.variant(s, sp.unit)
        operator = ("owner_or_operator_transfer"
                    if self.config.support_operator
                    else "owner_transfer")
        v = sp.record(
            operator = uv(operator),
            receiver = uv("owner_no_hook"),
            sender = uv("owner_no_hook"),
            custom = sp.none
            )
        v = sp.set_type_expr(v, self.get_type())
        return v

## The set of all tokens is represented by a `nat` if we assume that token-ids
## are consecutive, or by an actual `(set nat)` if not.
##
## - Knowing the set of tokens is useful for throwing accurate error messages.
## - Previous versions of the specification required this set for functional
##   behavior (operators worked per-token).
class Token_id_set:
    def __init__(self, config):
        self.config = config
    def empty(self):
        if self.config.assume_consecutive_token_ids:
            # The "set" is its cardinal.
            return sp.nat(0)
        else:
            return sp.set(t = token_id_type)
    def add(self, metaset, v):
        if self.config.assume_consecutive_token_ids:
            metaset.set(sp.max(metaset, v + 1))
        else:
            metaset.add(v)
    def contains(self, metaset, v):
        if self.config.assume_consecutive_token_ids:
            return (v < metaset) 
        else:
            metaset.contains(v)

##
## ## Implementation of the Contract
##
## `mutez_transfer` is an optional entry-point, hence we define it “outside” the
## class:
def mutez_transfer(contract, params):
    sp.verify(sp.sender == contract.data.administrator)
    sp.set_type(params.destination, sp.TAddress)
    sp.set_type(params.amount, sp.TMutez)
    sp.send(params.destination, params.amount)
##
## The `FA2` class builds a contract according to an `FA2_config` and an
## administrator address.
## It is inheriting from `FA2_core` which implements the strict
## standard and a few other classes to add other common features.
##
## - We see the use of
##   [`sp.entry_point`](https://www.smartpy.io/reference.html#_entry_points)
##   as a function instead of using annotations in order to allow
##   optional entry points.
## - The storage field `metadata_string` is a placeholder, the build
##   system replaces the field annotation with a specific version-string, such
##   as `"version_20200602_tzip_b916f32"`: the version of FA2-smartpy and
##   the git commit in the TZIP [repository](https://gitlab.com/tzip/tzip) that
##   the contract should obey.
class FA2_core(sp.Contract):
    def __init__(self, config, **extra_storage):
        self.config = config
        self.error_message = Error_message(self.config)
        self.operator_set = Operator_set(self.config)
        self.operator_param = Operator_param(self.config)
        self.token_id_set = Token_id_set(self.config)
        self.ledger_key = Ledger_key(self.config)
        self.token_meta_data = Token_meta_data(self.config)
        self.permissions_descriptor_ = Permissions_descriptor(self.config)
        self.batch_transfer    = Batch_transfer(self.config)
        if  self.config.add_mutez_transfer:
            self.transfer_mutez = sp.entry_point(mutez_transfer)
        if  self.config.add_permissions_descriptor:
            def permissions_descriptor(self, params):
                sp.set_type(params, sp.TContract(self.permissions_descriptor_.get_type()))
                v = self.permissions_descriptor_.make()
                sp.transfer(v, sp.mutez(0), params)
            self.permissions_descriptor = sp.entry_point(permissions_descriptor)
        if config.lazy_entry_points:
            self.add_flag("lazy_entry_points")
        if config.lazy_entry_points_multiple:
            self.add_flag("lazy_entry_points_multiple")
        self.add_flag("initial-cast")    
        sub_type = sp.TMap(sp.TString, sp.TBytes)
        
        self.init(
            ledger =
                self.config.my_map(tvalue = Ledger_value.get_type()),
            #tvalue is token_meta_data type, however this is not tzip-16 token metadata, below data is still necessary because tzip-16 metadata is only bytes 
            tokens =
                self.config.my_map(tvalue = self.token_meta_data.get_type()),
            operators = self.operator_set.make(),
            all_tokens = self.token_id_set.empty(),
            all_collections = self.token_id_set.empty(),
            metadata_string = sp.unit,
            collections = self.config.my_map(tkey=sp.TNat, tvalue = sp.TList(sp.TNat)),
            auctions = sp.map({}),
            max_editions = 25,
            max_royalty = 50,
            x = 0,
            # in percentage
            bidding_fee = 4,
            trading_fee = 25,
            trading_fee_collector = sp.address('tz1ijy2Z7sSmmxPqsZ6iGRn1ZHrCtpwR7XNm'),
            id_max_increment = 100,
            # token metadata tzip-12/tzip-16
            token_metadata = sp.big_map(tkey = sp.TNat,tvalue = sp.TRecord(token_id = sp.TNat, token_info = sub_type)),
            # contract metadata tzip-12/tzip-16
            # removed here to reduce the code size, need to be set by admin after deployment
            # https://gist.githubusercontent.com/harshabakku/642aba83d0c5dc9f2c3b9f645429b9d6/raw/87e38e40cfa9b3a0dd5d24b4802126351ebac84c/metadata.json
            metadata = sp.big_map({"":sp.bytes_of_string("")},tvalue = sp.TBytes),
            **extra_storage
        )

    @sp.entry_point
    def set_contract_metadata_uri(self,params):
        sp.verify(self.is_administrator(sp.sender))
        sp.set_type(params, sp.TBytes)
        self.data.metadata[""] = params

    @sp.entry_point
    def set_max_editions(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.max_editions = params
        
    @sp.entry_point
    def set_max_royalty(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.max_royalty = params    
    
    @sp.entry_point
    def set_trading_fee(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.trading_fee = params

    @sp.entry_point
    def set_bidding_fee(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.bidding_fee = params        
        
    @sp.entry_point
    def set_trading_fee_collector(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.trading_fee_collector = params
        
        
    @sp.entry_point
    def set_ipfs_registry(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.ipfs_registry = params
    
    @sp.entry_point
    def set_auction_factory(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.auction_factory = params
        
    @sp.entry_point   
    def set_token_metadata_uri(self, params):    
        from_user = self.ledger_key.make(sp.sender, params.token_id)
        
        sp.verify((self.data.ledger[from_user].balance >= 1),
                        message = self.error_message.not_owner())
        self.data.token_metadata[params.token_id].token_info[""] = params.uri
        
        
    # only nft owner can call this, turns on on_sale flag and sets token price 
    @sp.entry_point
    def list_token(self,params):
        # check if auction is going on
        # sp.verify(sp.sender == sp.source)
        sp.verify(~self.data.auctions.contains(params.token_id))
        nft = self.data.tokens[params.token_id]
        #check ledger balance >1 here instead of nft.owner, ledger balance is source of  truth
        # sp.verify(nft.owner == sp.sender, message = self.error_message.not_owner()) 
        # when using sp.source to identify tx initatior address results in the contract address,
        # from_user = self.ledger_key.make(sp.source, params.token_id)
        from_user = self.ledger_key.make(sp.sender, params.token_id)
        
        sp.verify((self.data.ledger[from_user].balance >= 1))
        nft.price = params.price
        nft.on_sale = True
    
    
    # only nft owner can call this, turns on on_sale flag and sets token price 
    @sp.entry_point
    def delist_token(self,params):
        # sp.verify(sp.sender == sp.source)

        # check if auction is going on 
        sp.verify(~self.data.auctions.contains(params.token_id))
        nft = self.data.tokens[params.token_id]
        #check ledger balance >1 here instead of nft.owner, ledger balance is source of  truth
        from_user = self.ledger_key.make(sp.sender, params.token_id)
        
        sp.verify((self.data.ledger[from_user].balance >= 1))
        nft.on_sale = False
    
    @sp.entry_point
    def bid(self,token_id):
        # sp.verify(sp.sender == sp.source)
        target_contract = sp.contract(sp.TUnit,
        self.data.auctions[token_id],
        entry_point="bid").open_some()
         
        # send bid fee to platform address
        # 1000 is used to facilitate decimals, value of 5 for trading_fee is basically 0.5 percent
        bidding_fee = sp.split_tokens(sp.amount, self.data.bidding_fee, 1000)
        sp.send(self.data.trading_fee_collector,bidding_fee)
        
        # sp.sender is the bidder here
        sp.transfer(sp.unit,sp.amount-bidding_fee,target_contract)
    
    @sp.entry_point
    def resolve_auction(self,token_id):
        # sp.verify(sp.sender == sp.source)
        target_contract = sp.contract(sp.TUnit,
        self.data.auctions[token_id],
        entry_point="resolve_auction").open_some()
        
        sp.transfer(sp.unit,sp.amount,target_contract)
        # transfer ownership of nft and also
        # collect tradingFees and royalty_fees and send the rest to owner 
        # in the cyclic call made by auction contract back to the end_auction in master_auction_contract/current_contract
    
    
    @sp.entry_point
    def cancel_auction(self,token_id):
        # sp.verify(sp.sender == sp.source)
        target_contract = sp.contract(sp.TUnit,
        self.data.auctions[token_id],
        entry_point="cancel_auction").open_some()
        sp.transfer(sp.unit,sp.amount,target_contract)
        del self.data.auctions[token_id]
        self.data.tokens[token_id].on_auction = False

             
    # swaps NFT and highest_bidder_funds and process trading fees and royalty fees and transfers ownership of NFT
    @sp.entry_point
    def end_auction(self, params):
        sp.verify(sp.sender == self.data.auctions[params.token_id])
        param_type = sp.TRecord(highest_bidder= sp.TAddress, token_id = sp.TNat)
        sp.set_type(params,param_type)
        
        # sp.amount is highest bid
        self.data.tokens[params.token_id].price = sp.amount
        self.process_fees(token_id = params.token_id, nft_price = sp.amount)
        self.merely_transfer([
            sp.record(from_ = self.data.tokens[params.token_id].owner, txs = [sp.record(to_ = params.highest_bidder, token_id = params.token_id, amount = 1)])
        ]);
        self.data.tokens[params.token_id].on_auction = False
        del self.data.auctions[params.token_id]
        
        
        
    
    @sp.entry_point
    def register_auction(self,params):
        
        # only auction_factory can register auction
        sp.verify(sp.sender == self.data.auction_factory)
        sp.verify(self.data.tokens.contains(params.token_id))
        
        from_user = self.ledger_key.make(sp.source, params.token_id)
        
        sp.verify((self.data.ledger[from_user].balance >= 1))
        sp.verify(~self.data.auctions.contains(params.token_id))                
        
        # store the auction contract address for the token_id
        self.data.auctions[params.token_id] = params.auction_contract
        self.data.tokens[params.token_id].price = params.reserve_price
        self.data.tokens[params.token_id].on_auction = True


    # nft owner can call this
    @sp.entry_point
    def transfer(self,params):
        sp.set_type(params, self.batch_transfer.get_type())
        sp.for txParams in params:
            current_from = txParams.from_
        #   for the the owner transfer, txParams.from_ should match sp.sender 
        #   operators will come into picture later when dex contract is separately introduced.
        #   operators support need to be introduced now itself so that the logic in the new dex contract should be able to transfer the FA2 NFT in a decentralised way leveraging operators.

         
            sp.for trx in txParams.txs:
                    
                if self.config.support_operator:
                          sp.verify(
                            #   (self.is_administrator(sp.sender)) | //true dex
                              (current_from == sp.sender) |
                              self.operator_set.is_member(self.data.operators,
                                                          current_from,
                                                          sp.sender,
                                                          trx.token_id),
                              message = self.error_message.not_operator())
                else:
                          sp.verify(
                            #   (self.is_administrator(sp.sender)) | //true dex
                              (current_from == sp.sender),
                              message = self.error_message.not_owner())                  
                sp.verify(~self.data.auctions.contains(trx.token_id))  
                # after the check of operator or owner now transfer the tokens
                self.merely_transfer([
            sp.record(from_ = txParams.from_, txs = [sp.record(to_ = trx.to_, token_id = trx.token_id, amount = trx.amount)])
        ]);
                    


    # process trading fees, royalty fees and transfers the rest of buyer's funds to nft owner
    def process_fees(self, token_id, nft_price):
        
        nft = self.data.tokens[token_id]
        
        nft_royalty_percent = nft.creator_royalty
        nft_creator = nft.creator
        
        
        #sp.split_tokens(amount, quantity, totalQuantity)
        # Computes amount * quantity / totalQuantity
        
        # send trading fee to platform address
        # 1000 is used to facilitate decimals in trading_fee, value of 25 for trading_fee is basically 2.5 percent
        trading_fee = sp.split_tokens(nft_price, self.data.trading_fee, 1000)
        sp.send(self.data.trading_fee_collector,trading_fee)
        
        # send royalty fee to creator
        # note that sp.mutez(0) check before for sending trading_fee and seller_amount to intentionally avoid other users buying 0 price token. user can always transfer the token for free to whoever they want.
        royalty_fee = sp.split_tokens(nft_price, nft_royalty_percent,  100)
        sp.if (royalty_fee != sp.mutez(0)):
            sp.send(nft_creator,royalty_fee)
        
        # send the rest to seller/nft_owner
        seller_amount = nft_price-trading_fee-royalty_fee
        sp.send(nft.owner, seller_amount)
        
    
    
    #  buy(buyer) and transfer(nftowner/operator) can  call this internal function passing various checks in transfer() and buy() entry points  
    def merely_transfer(self, params):
        sp.set_type(params, self.batch_transfer.get_type())
        sp.for transfer in params:
           current_from = transfer.from_
           sp.for tx in transfer.txs:
                sp.verify(tx.amount > 0, message = "TRANSFER_OF_ZERO")
                
                sp.verify(self.data.tokens.contains(tx.token_id),
                          message = self.error_message.token_undefined())
                # If amount is 0 we do nothing now:
                sp.if (tx.amount > 0):
                    from_user = self.ledger_key.make(current_from, tx.token_id)
                    sp.verify(
                        (self.data.ledger[from_user].balance >= tx.amount),
                        message = self.error_message.insufficient_balance())
                    to_user = self.ledger_key.make(tx.to_, tx.token_id)
                    self.data.ledger[from_user].balance = sp.as_nat(
                        self.data.ledger[from_user].balance - tx.amount)
                    sp.if self.data.ledger.contains(to_user):
                        self.data.ledger[to_user].balance += tx.amount
                    sp.else:
                         self.data.ledger[to_user] = Ledger_value.make(tx.amount)
                    
                    
                    #also update the token metadata with owner, given a tokenId we can get the owner of the token,  
                    nft = self.data.tokens[tx.token_id]
                    nft.owner = tx.to_  
                    
                    # set on_sale flag to False right after transfer
                    nft.on_sale = False
                    #  tx.to_ has to be used above, because sp.sender is nft buyer when buy() entrypoint is called and when transfer is called sp.sender is nft owner and merely_transfer is always called with right params 
                    
                sp.else:
                    pass

    @sp.entry_point
    def balance_of(self, params):
        sp.set_type(params, Balance_of.entry_point_type())
        def f_process_request(req):
            user = self.ledger_key.make(req.owner, req.token_id)
            sp.verify(self.data.tokens.contains(req.token_id),
                      message = self.error_message.token_undefined())
            sp.if self.data.ledger.contains(user):
                balance = self.data.ledger[user].balance
                sp.result(
                    sp.record(
                        request = sp.record(
                            owner = sp.set_type_expr(req.owner, sp.TAddress),
                            token_id = sp.set_type_expr(req.token_id, sp.TNat)),
                        balance = balance))
            sp.else:
                sp.result(
                    sp.record(
                        request = sp.record(
                            owner = sp.set_type_expr(req.owner, sp.TAddress),
                            token_id = sp.set_type_expr(req.token_id, sp.TNat)),
                        balance = 0))
        res = sp.local("responses", params.requests.map(f_process_request))
        destination = sp.set_type_expr(params.callback,
                                       sp.TContract(Balance_of.response_type()))
        sp.transfer(res.value, sp.mutez(0), destination)


    # nft buyer places this transaction
    @sp.entry_point
    def buy(self, params):
        # sp.verify(sp.sender == sp.source)
        nft = self.data.tokens[params.token_id]
        sp.verify(~self.data.auctions.contains(params.token_id))
        
        sp.verify(nft.owner != sp.sender)
        sp.verify(nft.price <= sp.amount)
        sp.verify(nft.on_sale == True)
        
        self.process_fees(token_id = params.token_id, nft_price = sp.amount)
        # note that nft_price is the total price the buyer has to pay including the trading fee
        # however in reality 1 percent is coming from buyer and 1 percent is coming from seller(in this case as and when receiving funds from the buyer)
        
        # buyer can call this function only after paying the token price above
        # sp.sender is the buyer here, //truly decentralized
        
        # note that checking user balance of tokenId, >=1 in merely transfer ensures that  nft_owner  owns the token_id. 
                
        self.merely_transfer([
            sp.record(from_ = nft.owner, txs = [sp.record(to_ = sp.sender, token_id = params.token_id, amount = 1)])
        ]);
        
         

    @sp.entry_point
    def update_operators(self, params):
        sp.set_type(params, sp.TList(
            sp.TVariant(
                add_operator = self.operator_param.get_type(),
                remove_operator = self.operator_param.get_type())))
        if self.config.support_operator:
            sp.for update in params:
                with update.match_cases() as arg:
                    with arg.match("add_operator") as upd:
                        sp.verify(upd.owner == sp.sender)
                                #   (self.is_administrator(sp.sender))) //true dex
                        self.operator_set.add(self.data.operators,
                                              upd.owner,
                                              upd.operator,
                                              upd.token_id)
                    with arg.match("remove_operator") as upd:
                        sp.verify(upd.owner == sp.sender) 
                        self.operator_set.remove(self.data.operators,
                                                 upd.owner,
                                                 upd.operator,
                                                 upd.token_id)
        else:
            sp.failwith(self.error_message.operators_unsupported())

    # this is not part of the standard but can be supported through inheritance.
    def is_administrator(self, sender):
        return sp.bool(False)

class FA2_administrator(FA2_core):
    def is_administrator(self, sender):
        return sender == self.data.administrator

    @sp.entry_point
    def set_administrator(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.administrator = params


class FA2_mint(FA2_core):
    
    def index_ipfs_hash(self, params):
        

        
        # specify the type accepted by entry point as record
        entry_point_params_type = sp.TRecord(ipfs_hash = sp.TString, nft_contract = sp.TAddress, metadata = sp.TMap(sp.TString, sp.TString))
        # set the same current params type as well 
        sp.set_type(params, entry_point_params_type)
        
        target_contract = sp.contract(entry_point_params_type,
        self.data.ipfs_registry,
        entry_point="add_fa2_token").open_some()
        
        # need to create  param data as record that needs to be redirected
        entry_point_params_record = sp.record(ipfs_hash = params.ipfs_hash, nft_contract = params.nft_contract, metadata = params.metadata
            )
        
        # call the entry point through proxy redirect
        sp.transfer(entry_point_params_record,sp.mutez(0),target_contract)           

    def string_of_nat(self, params):
        # sp.set_type(params, sp.TNat)

        c   = sp.map({x : str(x) for x in range(0, 10)})
        x   = sp.local('x', params)
        res = sp.local('res', [])
        sp.if x.value == 0:
            res.value.push('0')
        sp.while 0 < x.value:
            res.value.push(c[x.value % 10])
            x.value //= 10
        return sp.concat(res.value)
        
    
    
    @sp.entry_point
    def mint(self, params):
        
        # sp.verify(sp.sender == sp.source)
        # minter(sp.sender) is the creator and owner here
        # sp.verify(self.is_administrator(sp.sender))
        # Not really , we aim to be truly decentralized and trustless
        sp.verify(params.editions >=1)
        
        sp.verify(params.editions <= self.data.max_editions)
        
        sp.verify(params.creator_royalty <= self.data.max_royalty)
        

        sp.if params.editions == 1:
            sp.verify( params.collection_id== 0)
        sp.else :
            #token_id_set is a class that allows us manage a set very well
            sp.verify(~ self.token_id_set.contains(self.data.all_collections, params.collection_id),
                      "collection_id_exists")
            sp.verify(params.collection_id < self.data.all_collections + self.data.id_max_increment, "refer all_collections")          
        
        token_id = params.token_id
        
        
        # note that for limited editions ipfs_hash is indexed only once
        self.index_ipfs_hash(sp.record(ipfs_hash = params.ipfs_hash,nft_contract = sp.self_address, metadata = {
                    "token_id": self.string_of_nat(token_id),
                    "standard": "FA2"
            }))
        
        sp.set_type(params.editions, sp.TNat)
        self.token_id_set.add(self.data.all_collections, params.collection_id)
        
        # temp variable used to persist data in for loop,  
        self.data.x = sp.nat(0)       

        sp.for token_id in sp.range(token_id, token_id + params.editions):
              # note that if x is used instead of self.data.x, x is being reset to 0 for each iteration, basically for loop is not saving the incremented x here.
            # x is used here to increment edition_number here
            self.data.x += 1
            if self.config.non_fungible:
                # remove params.amount, use editions to control the no. of editions, each token_id always has only 1 amount
                # sp.verify(params.amount == 1, "NFT-asset: amount <> 1")
                sp.verify(~ self.token_id_set.contains(self.data.all_tokens, token_id))
                sp.verify(~self.data.tokens.contains(token_id))
                sp.verify(params.token_id < self.data.all_tokens + self.data.id_max_increment, "refer all_tokens")          
            user = self.ledger_key.make(sp.sender, token_id)
            self.token_id_set.add(self.data.all_tokens, token_id)
            
            
            sp.if self.data.ledger.contains(user):
                sp.verify(True,"token_id_exists")
            sp.else:
                self.data.ledger[user] = Ledger_value.make(1)
            
            #maintain collection map with list of token_id in each collection 
            
            sp.if params.editions > 1:
                sp.if self.data.collections.contains(params.collection_id):
                    self.data.collections[params.collection_id].push(token_id)
                sp.else:
                    self.data.collections[params.collection_id] = sp.list([token_id])
            #  create token_metadata first
            #  token_metadata_uri is a valid json uri or ipfs uri contain all metadata of NFT token and more offline fields             
            self.data.token_metadata[token_id] = sp.record(token_id = token_id, token_info = sp.map({"":params.token_metadata_uri, "decimals":sp.bytes_of_string("0") }))
            
            #  sp.sender is the owner, so minting if done by contracts, contract_address becomes owner
            self.data.tokens[token_id] = sp.record(
                         token_id = token_id,
                         
                         price = params.price,
                         ipfs_hash = params.ipfs_hash,
                         
                         on_sale = params.on_sale, 
                        #  on_auction always has higher preference than on_sale
                         on_auction = False,
                         owner = sp.sender,
                         decimals = 0,
                         extras = {
		            "category": params.category, 
		            "keywords": params.keywords,
		            "creator_name" : params.creator_name,
		            "name" : params.name,
		            "symbol"  : params.symbol,
		            "collection_name": params.collection_name
		    		},
                         collection_id = params.collection_id,
                         creator = sp.sender,
                         creator_royalty = params.creator_royalty,
                         edition_number = self.data.x,
                         editions = params.editions
                     );
                            
                     

class Auction_factory(sp.Contract): 
    def __init__(self, admin):
        self.auction = Auction()
        self.init(
            administrator = admin,
            nft_dex_contract = sp.address("KT1VEkFqqjoCCjpqn6EMDk4zbHdM6VxZrGcr")
        )
    
    @sp.entry_point
    def set_administrator(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.administrator = params
        
    def is_administrator(self, sender):
        return sender == self.data.administrator
    
        
    def is_dex_contract(self, sender):
        return sender == self.data.nft_dex_contract

   # need to update set_dex_contract every upgrade
    @sp.entry_point
    def set_dex_contract(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.nft_dex_contract = params
    

    # min_increase is sp.nat, the mutez amount
    # round_time is in seconds 
    @sp.entry_point
    def create_auction(self, params):
        sp.verify(sp.sender == sp.source) 
        
        # only owner can create auction contract(these checks are in nft_dex_contract.register_auction) 
        auction_contract = sp.some(sp.create_contract(
            storage = sp.record(owner = sp.sender,
                master_auction_contract = self.data.nft_dex_contract,
                asset_id = params.token_id,
                current_bid = params.reserve_price,
                min_increase = params.min_increase,
                highest_bidder = sp.to_address(sp.self),
                started = sp.bool(True),
                first_bid_placed = sp.bool(False),
                ended = sp.bool(False),
                start_time = sp.now,
                round_time = params.round_time), 
            contract = self.auction))
        
        # register auction contract address for the token_id
        # includes various checks required for token_id and owner
        target_contract = sp.contract(sp.TRecord(token_id = sp.TNat, auction_contract = sp.TAddress, reserve_price = sp.TMutez ),
        self.data.nft_dex_contract,
        entry_point="register_auction").open_some()
        
        sp.transfer(sp.record(token_id = params.token_id, auction_contract = auction_contract.open_some(), reserve_price = params.reserve_price),sp.amount,target_contract) 
        
           
class NFT_IPFS_registry(sp.Contract): 
    def __init__(self, admin):
        metadta_type = sp.TMap(sp.TString, sp.TString)
        big_map_value_type = sp.TRecord(nft_contract = sp.TAddress
        ,metadata = metadta_type)
        
        self.init(
            #  should have the ability to index ipfs_hash of NFTs of any chain
           # Map(ipfs_hash, record(contract='', Map('token_id'='' 'standard'='FA2')))
            ipfs_registry =
                 sp.big_map(tkey=sp.TString, tvalue = big_map_value_type),
            administrator = admin,
            nft_dex_contract = sp.address("KT1VEkFqqjoCCjpqn6EMDk4zbHdM6VxZrGcr")
        )
    
  
    @sp.entry_point
    def set_administrator(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.administrator = params
        
    def is_administrator(self, sender):
        return sender == self.data.administrator
    
        
    def is_dex_contract(self, sender):
        return sender == self.data.nft_dex_contract

   # need to update set_dex_contract every upgrade
    @sp.entry_point
    def set_dex_contract(self, params):
        sp.verify(self.is_administrator(sp.sender))
        self.data.nft_dex_contract = params
    
        
    @sp.entry_point
    def add_fa2_token(self, params):
        sp.verify(self.is_administrator(sp.sender) | self.is_dex_contract(sp.sender), message = 'admin or dex_contract can only add_fa2_token ')
        sp.verify(~self.data.ipfs_registry.contains(params.ipfs_hash))
        
        sp.set_type(params.metadata, sp.TMap(sp.TString, sp.TString))
        
        self.data.ipfs_registry[params.ipfs_hash] = sp.record(nft_contract = params.nft_contract, metadata = params.metadata)
    
    @sp.entry_point
    def remove_fa2_token(self, params):
        sp.verify(self.is_administrator(sp.sender) | self.is_dex_contract(sp.sender),  message = 'admin or dex_contract can only delete fa2_token ')
        sp.verify(self.data.ipfs_registry.contains(params.ipfs_hash),  message = 'ipfs_hash does not exist in NFT registry')
        
        del self.data.ipfs_registry[params.ipfs_hash]
    

class FA2(FA2_mint, FA2_administrator, FA2_core):
    def __init__(self, config, admin, ipfs_registry, auction_factory):
        FA2_core.__init__(self, config, paused = False, administrator = admin, ipfs_registry = ipfs_registry, auction_factory = auction_factory)

class Auction(sp.Contract):
    def __init__(self):
        self.init_type(t = sp.TRecord(owner = sp.TAddress,
                master_auction_contract = sp.TAddress,
                asset_id = sp.TNat,
                current_bid = sp.TMutez,
                min_increase = sp.TNat,
                highest_bidder = sp.TAddress,
                started = sp.TBool,
                first_bid_placed = sp.TBool,
                ended = sp.TBool,
                start_time = sp.TTimestamp,
                round_time = sp.TInt
                                ))

    @sp.entry_point
    def bid(self):
        sp.verify(self.data.started)
        sp.verify(~self.data.ended)
        bidder = sp.source
        sp.verify(~(self.data.owner == bidder),"bidder matches NFT owner")
        
        sp.verify(~(self.data.highest_bidder == bidder),"bidder matches highest bidder of current round")
        sp.verify(sp.amount >= self.data.current_bid + sp.utils.nat_to_mutez(self.data.min_increase))

        
        # external check is being done in nft_dex_contract 
        # verify an externally owned account and not a contract account 
        # sp.verify(sp.sender == sp.source | sp.sender == self.data.master_auction_contract )
        
        # only master auction contract (NFT contract) can call 
        sp.verify(sp.sender == self.data.master_auction_contract )
        
        
        # verify now is less than round_end_time = start_time + round_time
        sp.verify(sp.now < self.data.start_time.add_seconds(self.data.round_time),"auction finished")
        
        sp.if ~self.data.first_bid_placed:
            self.data.first_bid_placed = sp.bool(True)
        sp.else:
            sp.send(self.data.highest_bidder, self.data.current_bid)
        self.data.current_bid = sp.amount
        self.data.highest_bidder = bidder
        

    @sp.entry_point
    def resolve_auction(self):
        sp.verify(self.data.started)
        sp.verify(self.data.first_bid_placed)
        sp.verify(~self.data.ended)
        
        # only master auction contract (NFT contract) can call 
        sp.verify(sp.sender == self.data.master_auction_contract)
        
        
        sp.verify((sp.source == self.data.highest_bidder) | (sp.source == self.data.owner), "only nft owner or highest_bidder can resolve auction")
        
        
        # nft owner can resolve at any time and take the highest bid
        
        # highest bidder can only resolve after round_time / auction_end_time
        # verify now more than round_end_time = start_time + round_time
        
        sp.if (sp.source == self.data.highest_bidder):
            sp.verify(sp.now > self.data.start_time.add_seconds(self.data.round_time), "highest bidder can only resolve after round finishes")
        
        
        # cant do this below we need to collect trading fees and royalty first 
        # master_auction_contract.end_auction() is called to take care of this
        
        # send current_bid to it's bidder (the current owner)
        # sp.send(self.data.owner, self.data.current_bid)
        
        self.data.owner = self.data.highest_bidder
        
        target_contract = sp.contract(sp.TRecord(highest_bidder= sp.TAddress, token_id = sp.TNat),
        self.data.master_auction_contract,
        entry_point="end_auction").open_some()
        
        sp.transfer(sp.record(highest_bidder = self.data.highest_bidder,token_id = self.data.asset_id),self.data.current_bid,target_contract)
        self.data.ended = sp.bool(True)
        
        
    

    @sp.entry_point
    def cancel_auction(self):
        sp.verify(self.data.started)
        sp.verify(~self.data.ended)
        sp.verify(sp.sender == self.data.master_auction_contract)
        
        sp.verify(sp.source == self.data.owner, "only nft owner can cancel auction")
        
        # can cancel auction if no bids are placed.
        # if there are bids, can only cancel before the round finishes  
        sp.if self.data.first_bid_placed:
            sp.verify(sp.now < self.data.start_time.add_seconds(self.data.round_time), "round has finished with bids placed")
            
            # send current_bid to highest_bidder
            sp.send(self.data.highest_bidder, self.data.current_bid)
        
        
        self.data.current_bid = sp.mutez(0)
        self.data.highest_bidder = sp.sender
        self.data.ended = sp.bool(True)






## ## Tests
##
## ### Auxiliary Consumer Contract
##
## This contract is used by the tests to be on the receiver side of
## callback-based entry-points.
## It stores facts about the results in order to use `scenario.verify(...)`
## (cf.
##  [documentation](https://www.smartpy.io/reference.html#_in_a_test_scenario_)).
class View_consumer(sp.Contract):
    def __init__(self, contract):
        self.contract = contract
        self.init(last_sum = 0,
                  last_acc = "",
                  operator_support =  not contract.config.support_operator)

    @sp.entry_point
    def receive_balances(self, params):
        sp.set_type(params, Balance_of.response_type())
        self.data.last_sum = 0
        sp.for resp in params:
            self.data.last_sum += resp.balance
    
    @sp.entry_point
    def receive_permissions_descriptor(self, params):
        self.contract.permissions_descriptor_.set_type_and_layout(params)
        sp.if params.operator.is_variant("owner_or_operator_transfer"):
            self.data.operator_support = True
        sp.else:
            self.data.operator_support = False


## ### Generation of Test Scenarios
##
## Tests are also parametrized by the `FA2_config` object.
## The best way to visualize them is to use the online IDE
## (<https://www.smartpy.io/ide/>).
def add_test(config, is_default = True):
  
    
    @sp.add_test(name = config.name, is_default = is_default)
    def test():
        scenario = sp.test_scenario()
        scenario.h1("FA2 Contract Name: " + config.name)
        scenario.table_of_contents()
        # sp.test_account generates ED25519 key-pairs deterministically:
        admin = sp.test_account("Administrator")
        alice = sp.test_account("Alice")
        bob   = sp.test_account("Robert")
        bob2   = sp.test_account("Robert2")
        bob3   = sp.test_account("Robert3")
        
        
        scenario.h1("Use to deploy IPFS_registry contract to Delphi ")
        
        # admin address is the param here        
        ipfs_registry = NFT_IPFS_registry( sp.address('tz1SotrT1MEeC8bNeGzgBLUb3ryfASsFsdun'))
        scenario += ipfs_registry
        
        scenario.h1("Use to deploy Auction_factory contract to Delphi ")
        
        # admin address is the param here        
        auction_factory = Auction_factory(sp.address('tz1SotrT1MEeC8bNeGzgBLUb3ryfASsFsdun'))
        scenario += auction_factory
        
         
        scenario.h1("Use to deploy FA2-NFTToken to Delphi testnet")
        
        # used for contract manual origination, third param is ipfs_registry contract address and fourth is auction_factory which should already be deployed on chain
        c0 = FA2(config, sp.address('tz1SotrT1MEeC8bNeGzgBLUb3ryfASsFsdun'),sp.address('KT19SkRUVM6c1gZ2Whrgq6TJmsWf2d3ggPyN'),sp.address('KT1LBb65XT1xoj9xutpomFhDWAevYSG8i5H9'))
        scenario += c0
        
        
        scenario.h1(" Test IPFS registry Contract")
    
        # deploy ipfs registry, auction_factory first, and use their address' and deploy dex contract, set dex contract address in ipfs registry and auction_factory contract again
        ipfs_registry =  NFT_IPFS_registry(admin.address)
        scenario += ipfs_registry
        
        auction_factory =  Auction_factory(admin.address)
        scenario += auction_factory
        
        c1 = FA2(config, admin.address, ipfs_registry.address, auction_factory.address)
        scenario += c1
        
        scenario.h3("Bob fails to set dex_contract")
        scenario += auction_factory.set_dex_contract(c1.address).run(valid = False,sender=bob,amount=sp.mutez(0))
        
        scenario.h3("Admin sets dex_contract in Auction_factory")
        scenario += auction_factory.set_dex_contract(c1.address).run(sender=admin,amount=sp.mutez(0))
        
        
        scenario.h3("Bob fails to set dex_contract")
        scenario += ipfs_registry.set_dex_contract(c1.address).run(valid = False,sender=bob,amount=sp.mutez(0))
        
        scenario.h3("Admin sets dex_contract")
        scenario += ipfs_registry.set_dex_contract(c1.address).run(sender=admin,amount=sp.mutez(0))
        
        
        scenario.h3("Bob fails to add fa2 token")
        scenario += ipfs_registry.add_fa2_token(ipfs_hash = "ipfshash1", nft_contract = c1.address, metadata = {
                    "token_id": "0",
                    "standard": "FA2"
            }).run(valid = False,sender=bob,amount=sp.mutez(0))
        
        scenario.h3("Admin adds  fa2 token")
        scenario += ipfs_registry.add_fa2_token(ipfs_hash = "ipfshash1",nft_contract = c1.address, metadata = {
                    "token_id": "0",
                    "standard": "FA2"
            }).run(sender=admin,amount=sp.mutez(0))
        
        scenario.h3("Admin fails to add existing  fa2 token")
        scenario += ipfs_registry.add_fa2_token(ipfs_hash = "ipfshash1",nft_contract = c1.address, metadata = {
                    "token_id": "0",
                    "standard": "FA2"
            }).run(valid = False, sender=admin,amount=sp.mutez(0))
        
        scenario.h3("Bob fails to remove fa2 token")
        scenario += ipfs_registry.remove_fa2_token(ipfs_hash = "ipfshash1").run(sender=bob,valid = False)
        
        scenario.h3("Admin removes fa2 token")
        scenario += ipfs_registry.remove_fa2_token(ipfs_hash = "ipfshash1").run(sender=admin,amount=sp.mutez(0))
      
      
        
        # Let's display the accounts:
        scenario.h2("Accounts")
        scenario.show([admin, alice, bob])
     
        scenario.h2("Initial Minting")
        scenario.h2("Alice mints 1 NFT ")

# c1 is already deployed with appropriate ipfs_registry address and dex_contract is set in ipfs_registry as well
        scenario += c1.mint(symbol = 'NFT',
                            # initial price set but on_sale is false
                            price = sp.mutez(20000),
                            ipfs_hash = 'QmPfSVrTm8WZzJuy1Kksm6u5P8gQAPfDAQJn9tzCnW2ynW',
                            name = "NFT Name",
                            token_id = 1,
                            on_sale = True,
                            category = "Art",
                            keywords = "avengers,nft,superhero,movie",
                            creator_royalty = 0, 
                            #creator_royalty= 0 could not be bought on edo2net, test case added
                            creator_name = "test_creator",
                            collection_id = 0,
                            collection_name = "",
                            token_metadata_uri = sp.bytes_of_string("ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o"),                        
                            editions = 1).run(sender = alice)
        scenario.verify(
            c1.data.tokens[1].creator == alice.address)
        
        scenario.h2("The Creator fails to mints 1 NFT with 51 royalty to Alice.")

        scenario += c1.mint(symbol = 'NFT',
                            # initial price set but on_sale is false
                            price = sp.mutez(20000),
                            ipfs_hash = 'QmPfSVrTm8WZzJuy1Kksm6u5P8gQAPfDAQJn9tzCnW2ynW',
                            name = "NFT Name",
                            token_id = 98,
                            on_sale = True,
                            category = "Art",
                            keywords = "avengers,nft,superhero,movie",
                            creator_royalty = 51,
                            creator_name = "test_creator",
                            collection_id = 0,
                            collection_name = "",
                            token_metadata_uri = sp.bytes_of_string("ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o"),                        
                            editions = 1).run(valid = False, sender = admin)
        
        scenario.h2("The administrator fails to mint same token_id")
        scenario += c1.mint(symbol = 'NFT',
                        # initial price set but on_sale is false
                        price = sp.mutez(20000),
                        ipfs_hash = 'QmPfSVrTm8WZzJuy1Kksm6u5P8gQAPfDAQJn9tzCnW2ynW',
                        name = "NFT Name",
                        token_id = 1,
                        on_sale = True,
                        category = "Art",
                        keywords = "avengers,nft,superhero,movie",
                        collection_id = 0,
                        collection_name = "",
                        creator_royalty = 49,
                        creator_name = "test_creator",
                        token_metadata_uri = sp.bytes_of_string("ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o"),
                        editions = 1).run(valid = False, sender = admin)                    
        scenario.h2("The administrator fails to mint token_id incremented too high to 102")
        scenario += c1.mint(symbol = 'NFT',
                        # initial price set but on_sale is false
                        price = sp.mutez(20000),
                        ipfs_hash = 'QmPfSVrTm8WZzJuy1Kksm6u5P8gQAPfDAQJn9tzCnW2ynW',
                        name = "NFT Name",
                        token_id = 102,
                        on_sale = True,
                        category = "Art",
                        keywords = "avengers,nft,superhero,movie",
                        creator_royalty = 49,
                        creator_name = "test_creator",
                        collection_id = 0,
                        collection_name = "",
                        token_metadata_uri = sp.bytes_of_string("ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o"),
                        editions = 1).run(valid = False, sender = admin)   
                        
                        
        scenario.h2(" Alice updates Token Metadata uri")

        scenario += c1.set_token_metadata_uri(token_id = 1, uri = sp.bytes_of_string("new_ipfs_uri")).run(sender = alice)
        
        scenario.verify(
            c1.data.token_metadata[1].token_info[""] == sp.bytes_of_string("new_ipfs_uri"))
        
        scenario.h2("Bob cant Update Token Metadata uri of Alice token_id:1")

        scenario += c1.set_token_metadata_uri(token_id = 1, uri = sp.bytes_of_string("new_ipfs_uri")).run(sender = bob, valid = False)
        
        scenario.h2("Mint Limited Edition NFT")
        scenario += c1.mint(symbol = 'NFT',
                            # initial price set but on_sale is false
                            price = sp.mutez(20000),
                            ipfs_hash = 'PfSVrTm8WZzJuy1Kksm6u5P8gQAPfDAQJn9tzCnW2ynW',
                            name = "NFT Name",
                            token_id = 2,
                            on_sale = True,
                            category = "Art",
                            keywords = "avengers,nft,superhero,movie",
                            collection_id = 1,
                            collection_name = "First Collection",
                            creator_royalty = 30,
                            creator_name = "test_creator",
                            token_metadata_uri = sp.bytes_of_string("ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o"),                            
                            editions = 10).run(sender = bob)
        scenario.verify(
            c1.data.tokens[3].edition_number == 2)
        scenario.verify(
            c1.data.token_metadata[3].token_info[""] ==sp.bytes_of_string('ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o'))    
        scenario.verify(
            c1.data.tokens[3].on_auction
            == False)    
        scenario.h2("Fail Mint existing collection_id Limited Edition NFT")
        scenario += c1.mint(symbol = 'NFT',
                            # initial price set but on_sale is false
                            price = sp.mutez(20000),
                            ipfs_hash = 'PfSVrTm8WZzJuy1Kksm6u5P8gQAPfDAQJn9tzCnW2ynW',
                            name = "NFT Name",
                            token_id = 2,
                            on_sale = True,
                            category = "Art",
                            keywords = "avengers,nft,superhero,movie",
                            creator_royalty = 49,
                            creator_name = "test_creator",
                            collection_id = 1,
                            collection_name = "First Collection",
                            token_metadata_uri = sp.bytes_of_string("ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o"),                            
                            editions = 10).run(valid = False, sender = admin)    
        
        scenario.h2("Fail Mint  collection_id incremented too high to 102")
        scenario += c1.mint(symbol = 'NFT',
                            # initial price set but on_sale is false
                            price = sp.mutez(20000),
                            ipfs_hash = 'PfSVrTm8WZzJuy1Kksm6u5P8gQAPfDAQJn9tzCnW2ynW',
                            name = "NFT Name",
                            token_id = 2,
                            on_sale = True,
                            category = "Art",
                            keywords = "avengers,nft,superhero,movie",
                            creator_royalty = 49,
                            creator_name = "test_creator",
                            collection_id = 102,
                            collection_name = "First Collection",
                            token_metadata_uri = sp.bytes_of_string("ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o"),                            
                            editions = 2).run(valid = False, sender = admin)           
        scenario.h2("Can Mint Non Consecutive/increasing token_id = 16, with all_tokens= 12")
        scenario += c1.mint(symbol = 'NFT',
                            # initial price set but on_sale is false
                            price = sp.mutez(20000),
                            ipfs_hash = 'VrTm8WZzJuy1Kksm6u5P8gQAPfDAQJn9tzCnW2ynW',
                            name = "NFT Name",
                            token_id = 16,
                            on_sale = True,
                            category = "Art",
                            keywords = "avengers,nft,superhero,movie",
                            collection_id = 0,
                            collection_name = "",
                            creator_royalty = 49,
                            creator_name = "test_creator",
                            token_metadata_uri = sp.bytes_of_string("ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o"),                            
                            editions = 1).run(sender = admin)
                            
        scenario.h2("Cannot Mint Non Consecutive token_id =15 , with all_tokens = 17")
        scenario += c1.mint(symbol = 'NFT',
                            # initial price set but on_sale is false
                            price = sp.mutez(20000),
                            ipfs_hash = 'Juy1Kksm6u5P8gQAPfDAQJn9tzCnW2ynW',
                            name = "NFT Name",
                            token_id = 15,
                            on_sale = True,
                            category = "Art",
                            keywords = "avengers,nft,superhero,movie",
                            creator_royalty = 49,
                            creator_name = "test_creator",
                            collection_id = 0,
                            collection_name = "",
                            token_metadata_uri = sp.bytes_of_string("ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o"),                            
                            editions = 1).run(valid = False,sender = admin)                    
        scenario.h2(" Can Mint Limited Edition NFT with consecutive/increasing  collectionId= 4")
        scenario += c1.mint(symbol = 'NFT',
                            # initial price set but on_sale is false
                            price = sp.mutez(20000),
                            ipfs_hash = 'y1Kksm6u5P8gQAPfDAQJn9tzCnW2ynW',
                            name = "NFT Name",
                            token_id = 17,
                            on_sale = True,
                            category = "Art",
                            keywords = "avengers,nft,superhero,movie",
                            creator_royalty = 49,
                            creator_name = "test_creator",
                            collection_id = 4,
                            collection_name = "First Collection",
                            token_metadata_uri = sp.bytes_of_string("ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o"),
                            editions = 2).run(sender = admin)
        
        scenario.h2(" Cannot Mint Limited Edition NFT with non consecutive  collectionId= 2, when all_collections = 5")
        scenario += c1.mint(symbol = 'NFT',
                            # initial price set but on_sale is false
                            price = sp.mutez(20000),
                            ipfs_hash = '8WZzJuy1Kksm6u5P8gQAPfDAQJn9tzCnW2ynW',
                            name = "NFT Name",
                            token_id = 20,
                            on_sale = True,
                            category = "Art",
                            keywords = "avengers,nft,superhero,movie",
                            creator_royalty = 49,
                            creator_name = "test_creator",
                            collection_id = 2,
                            collection_name = "First Collection",
                            token_metadata_uri = sp.bytes_of_string("ipfs://QmWDcp3BpBjvu8uJYxVqb7JLfr1pcyXsL97Cfkt3y1758o"),                            
                            editions = 2).run(valid= False, sender = admin)                    
                            
        scenario.h2("Transfers Alice -> Bob")
        scenario += c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 1,
                                                  token_id = 1)
                                    ])
            ]).run(sender = alice)
        scenario.verify(
            c1.data.ledger[c1.ledger_key.make(alice.address, 1)].balance == 0)
        scenario.verify(
            c1.data.ledger[c1.ledger_key.make(bob.address, 1)].balance == 1)
        scenario.verify(
            c1.data.ledger[c1.ledger_key.make(alice.address, 1)].balance
            == 0)
        scenario.verify(
            c1.data.ledger[c1.ledger_key.make(bob.address, 1)].balance
            == 1)
        if config.single_asset:
            return
        
        scenario.h3("Delist NFT")
        scenario += c1.delist_token(token_id=1).run(sender=bob,
                                                amount=sp.mutez(0))
        scenario.verify(
            c1.data.tokens[1].on_sale == False)
        
        scenario.h3("Fail Buy NFT")
        scenario += c1.buy(token_id=1).run(sender=alice,
                                                amount=sp.mutez(1111000), valid = False)
        
        
        # buy NFT fails without listing NFT first
        scenario.h3("List NFT")
        scenario += c1.list_token(token_id=1, price=sp.mutez(11000)).run(sender=bob,amount=sp.mutez(0))
        
        scenario.h3("Buy NFT")
        scenario += c1.buy(token_id=1).run(sender=alice,
                                                amount=sp.mutez(11000))
        
       
        scenario.h3("Buy NFT-2")
        scenario += c1.buy(token_id=2).run(sender=alice,amount=sp.mutez(20000))
        
        
        #note that tezos balance of alice, bob and trading_fee_collector address need to be validated to have end-end fool proof test cases. no references/examples to access tez balance of test users
        #for now we can find the tx operations of trading-fee's transfer and transfer of nft-price to the seller in tests/Buy NFT
        # Transfer 0.000220tz to trading_fee_collector
        # Transfer 0.005390 (49 percent) as royalty to creator
        # Transfer 0.005390 (the rest ) to seller
        #note that basicallly we are trying to validate the percentage calculation code and the above verifies that 0.000220 tez is 2 percent of  0.011 tez
        
       
                                      
                                      
        scenario.h3("Multi-token Transfer Bob -> Alice")
        scenario += c1.transfer(
            [
                c1.batch_transfer.item(from_ = bob.address,
                                    txs = [
                                        sp.record(to_ = alice.address,
                                                  amount = 1,
                                                  token_id = 3)]),
                # We voluntarily test a different sub-batch:
                c1.batch_transfer.item(from_ = bob.address,
                                    txs = [
                                        sp.record(to_ = alice.address,
                                                  amount = 1,
                                                  token_id = 4)])
            ]).run(sender = bob)
        scenario.h2("Other Basic Permission Tests")
        scenario.h3("Bob cannot transfer Alice's tokens.")
        scenario += c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 1,
                                                  token_id = 2),
                                        sp.record(to_ = bob.address,
                                                  amount = 1,
                                                  token_id = 3)])
            ]).run(sender = bob, valid = False)
        scenario.h3("Admin can't transfer anything.")
        scenario += c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 1,
                                                  token_id = 2),
                                        sp.record(to_ = bob.address,
                                                  amount = 1,
                                                  token_id = 3)]),
                c1.batch_transfer.item(from_ = bob.address,
                                    txs = [
                                        sp.record(to_ = alice.address,
                                                  amount = 1,
                                                  token_id = 10)])
            ]).run(valid = False, sender = admin)
    
        scenario += c1.update_operators([]).run()
        scenario.h3("Operator Accounts")
        op0 = sp.test_account("Operator0")
        op1 = sp.test_account("Operator1")
        op2 = sp.test_account("Operator2")
        scenario.show([op0, op1, op2])
        scenario.h3("Admin cannot change Alice's operator..//true dex")
        scenario += c1.update_operators([
            sp.variant("add_operator", c1.operator_param.make(
                owner = alice.address,
                operator = op1.address,
                token_id = 2)),
            sp.variant("add_operator", c1.operator_param.make(
                owner = alice.address,
                operator = op1.address,
                token_id = 3))
        ]).run(valid = False, sender = admin)
        
        scenario.h3("Alice updates operators for tokens 2 and 3")
        scenario += c1.update_operators([
            sp.variant("add_operator", c1.operator_param.make(
                owner = alice.address,
                operator = op1.address,
                token_id = 2)),
            sp.variant("add_operator", c1.operator_param.make(
                owner = alice.address,
                operator = op1.address,
                token_id = 3))
        ]).run( sender = alice)
        
        
        
        scenario.h3("Operator1 can now transfer Alice's tokens 2")
        scenario += c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 1,
                                                  token_id = 2)])
            ]).run(sender = op1)
        scenario.h3("Operator1 cannot transfer Bob's tokens")
        scenario += c1.transfer(
            [
                c1.batch_transfer.item(from_ = bob.address,
                                txs = [
                                        sp.record(to_ = op1.address,
                                                  amount = 1,
                                                  token_id = 2)])
            ]).run(sender = op1, valid = False)
        scenario.h3("Operator2 cannot transfer Alice's tokens 3")
        scenario += c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 1,
                                                  token_id = 3)])
            ]).run(sender = op2, valid = False)
        scenario.h3("Alice can remove their operator")
        scenario += c1.update_operators([
            sp.variant("remove_operator", c1.operator_param.make(
                owner = alice.address,
                operator = op1.address,
                token_id = 2)),
            sp.variant("remove_operator", c1.operator_param.make(
                owner = alice.address,
                operator = op1.address,
                token_id = 3))
        ]).run(sender = alice)
        scenario.h3("Operator1 cannot transfer Alice's tokens any more")
        scenario += c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = op1.address,
                                                  amount = 1,
                                                  token_id = 3)])
            ]).run(sender = op1, valid = False)
        scenario.h3("Bob can add Operator0.")
        scenario += c1.update_operators([
            sp.variant("add_operator", c1.operator_param.make(
                owner = bob.address,
                operator = op0.address,
                token_id = 5)),
            sp.variant("add_operator", c1.operator_param.make(
                owner = bob.address,
                operator = op0.address,
                token_id = 6))
        ]).run(sender = bob)
        scenario.h3("Operator0 can transfer Bob's tokens '5' and '6'")
        scenario += c1.transfer(
            [
                c1.batch_transfer.item(from_ = bob.address,
                                    txs = [
                                        sp.record(to_ = alice.address,
                                                  amount = 1,
                                                  token_id = 5)]),
                c1.batch_transfer.item(from_ = bob.address,
                                    txs = [
                                        sp.record(to_ = alice.address,
                                                  amount = 1,
                                                  token_id = 6)])
            ]).run(sender = op0)
        scenario.h3("Bob cannot add Operator0 for Alice's tokens 3.")
        scenario += c1.update_operators([
            sp.variant("add_operator", c1.operator_param.make(
                owner = alice.address,
                operator = op0.address,
                token_id = 3
            ))
        ]).run(sender = bob, valid = False)
        scenario.h3("Alice can  add Operator0 for their tokens 3.")
        scenario += c1.update_operators([
            sp.variant("add_operator", c1.operator_param.make(
                owner = alice.address,
                operator = op0.address,
                token_id = 0
            ))
        ]).run(sender = alice, valid = True)
        
        scenario.h2("English Auction FA2 NFTs")
        scenario.h3("Bob fails to create english auction contract for token_id 1(Alice's Token).")
        
        token_id = 1
        round_time = 60*60
        min_increase = 10000
        reserve_price = 50000
        
        # smartpy bug , actually fails in logs, already reported
        # scenario += auction_factory.create_auction(token_id = token_id, reserve_price = sp.mutez(reserve_price), min_increase = min_increase, round_time = round_time).run(sender = bob,source = bob, valid = False)
      
        scenario.h3("Alice can create english auction contract for token_id 1.")
        scenario += auction_factory.create_auction(token_id = token_id, reserve_price = sp.mutez(reserve_price), min_increase = min_increase, round_time = round_time).run(sender = alice, source = alice)
        scenario.verify(
            c1.data.auctions.contains(token_id))
        
        scenario.h3("Alice fails to create english auction contract for token_id 1(auction is going on).")
        
        # smartpy bug, actually fails in logs
        # scenario += auction_factory.create_auction(token_id = token_id, reserve_price = sp.mutez(reserve_price), min_increase = min_increase, round_time = round_time).run(sender = alice,source = alice, valid = False)
        
        
        # another smartpy bug #2 in smartpy the below calls always fails but when set as valid = False(it would say tx is valid)
        # same problem with all inter contract calls when tx fails in the inter contract call 
        
        scenario.h3("Bob fails to bid, min_increase not satisfied")
        #smartpy bug #2
        # scenario += c1.bid(token_id).run(sender = bob,source = bob,amount=sp.mutez(reserve_price+min_increase-1), valid = False)
        
        scenario.h3("Bob bids succesfully")
        scenario += c1.bid(token_id).run(sender = bob,source = bob,amount=sp.mutez(reserve_price+min_increase+1+240), valid = True)
       
        scenario.h3("Bob fails to bid, cause' bob is already highest bidder")
        #smartpy bug #2
        # scenario += c1.bid(token_id).run(sender = bob,source = bob,amount=sp.mutez(reserve_price+2*min_increase+1), valid = False)
        
        scenario.h3("Alice fails to bid, 'cause Alice is owner of NFT")
        #smartpy bug #2
        # scenario += c1.bid(token_id).run(sender = alice,source = alice,amount=sp.mutez(reserve_price+2*min_increase+1), valid = False)
       
        scenario.h3("Admin bids higher than Bob")
        scenario += c1.bid(token_id).run(sender = admin,source = admin,amount=sp.mutez(reserve_price+2*min_increase+1+80*4))
        
        scenario.h3("Bob can bid again")
        scenario += c1.bid(token_id).run(sender = bob,source = bob,amount=sp.mutez(reserve_price+3*min_increase+1+100*4))
        
        scenario.h3("Bob2 bids higher than Admin")
        scenario += c1.bid(token_id).run(sender = bob2,source = bob2,amount=sp.mutez(reserve_price+4*min_increase+1+120*4))
       
        scenario.h3("Bob3 bids higher than Bob2")
        scenario += c1.bid(token_id).run(sender = bob3,source = bob3,amount=sp.mutez(reserve_price+5*min_increase+1+140*4))
        
        # smartpy bug #2 (actually fails)
        # scenario.h3("Bob fails to resolve auction(not owner)")
        # scenario += c1.resolve_auction(token_id).run(sender = bob,source = bob,valid = False)
        
        scenario.verify(
            c1.data.tokens[token_id].on_auction
            ==True)
            
        scenario.h3("Alice(Owner) resolves auction and takes highest bid")
        scenario += c1.resolve_auction(token_id).run(sender = alice, source = alice)
        scenario.verify(
            c1.data.ledger[c1.ledger_key.make(alice.address, token_id)].balance
            == 0)
        scenario.verify(
            c1.data.ledger[c1.ledger_key.make(bob3.address, token_id)].balance
            == 1)
            
        scenario.verify(
            c1.data.tokens[token_id].on_auction
            == False)    
        
        # after resolve auction, nft_price shows as highest_bid  
        scenario.verify(
            c1.data.tokens[token_id].price
            >= sp.mutez(reserve_price+5*min_increase+1))    
        
        scenario.h3("Alice(Owner) fails to resolves auction(auction complete)")
        scenario += c1.resolve_auction(token_id).run(sender = alice,source = alice, valid= False)
        
        scenario.h3("bob2 fails to bid(auction complete)")
        scenario += c1.bid(token_id).run(sender = bob2, source = bob2, amount=sp.mutez(reserve_price+6*min_increase+1), valid= False)
        
        scenario.h3("Bob auctions token_id 2, highest_bidder resolves this time")
        # bidder can only resolve after round_time (auction_end_time) 
        
        round_time = 1
        scenario += auction_factory.create_auction(token_id = 2, reserve_price = sp.mutez(reserve_price), min_increase = min_increase, round_time = round_time).run(sender = bob, source = bob)
        
        # base_price sent for auction becomes nft_price
        scenario.verify(
            c1.data.tokens[2].price
            == sp.mutez(reserve_price))    
        
        scenario += c1.bid(2).run(sender = bob2,source = bob2,amount=sp.mutez(reserve_price+min_increase+1+60*4), valid = True)
       
        # highest_bidder can only resolve after round_time( 1 sec) can only be tested on delphi
        # scenario += c1.resolve_auction(2).run(sender = bob2, source= bob2)       
        
        # smartpy bug #2
        # scenario.h3("Alice(not owner) fails to cancel the auction")
        # scenario += c1.cancel_auction(2).run(sender = alice,source =alice valid = False) 
        
        scenario.h3("Bob/Owner cancels auction")  
        #note that bob2's bid is reverted here.
        scenario += c1.cancel_auction(2).run(sender = bob, source = bob)  
        scenario.verify(
            c1.data.tokens[2].on_auction
            == False)
        
        # bob can create auction again with diff auction params
        scenario += auction_factory.create_auction(token_id = 2, reserve_price = sp.utils.nat_to_mutez(reserve_price), min_increase = 2*min_increase, round_time = 2*round_time).run(sender = bob, source = bob)
        scenario.verify(
            c1.data.tokens[2].on_auction
            == True)
        scenario.h3("Bob/Owner cancels auction before any bids")  
        scenario += c1.cancel_auction(2).run(sender = bob,source = bob)  
        
        #######
                                                
        scenario.h2("Consumer Contract for Callback Calls.")
        consumer = View_consumer(c1)
        scenario += consumer
        scenario.p("Consumer virtual address: "
                   + sp.contract_address(consumer).export())
        scenario.h2("Balance-of.")
        def arguments_for_balance_of(receiver, reqs):
            return (sp.record(
                callback = sp.contract(
                    Balance_of.response_type(),
                    sp.contract_address(receiver),
                    entry_point = "receive_balances").open_some(),
                requests = reqs))
        scenario += c1.balance_of(arguments_for_balance_of(consumer, [
            sp.record(owner = alice.address, token_id = 1)
        ]))
        scenario += c1.balance_of(arguments_for_balance_of(consumer, [
            sp.record(owner = bob.address, token_id = 1)
        ]))
        scenario.verify(consumer.data.last_sum == 0)
        
        
##
## ## Global Environment Parameters
##
## The build system communicates with the python script through
## environment variables.
## The function `environment_config` creates an `FA2_config` given the
## presence and values of a few environment variables.
def global_parameter(env_var, default):
    try:
        if os.environ[env_var] == "true" :
            return True
        if os.environ[env_var] == "false" :
            return False
        return default
    except:
        return default

def environment_config():
    return FA2_config(
        debug_mode = global_parameter("debug_mode", False),
        single_asset = global_parameter("single_asset", False),
        non_fungible = global_parameter("non_fungible", True),
        add_mutez_transfer = global_parameter("add_mutez_transfer", False),
        readable = global_parameter("readable", True),
        force_layouts = global_parameter("force_layouts", True),
        support_operator = global_parameter("support_operator", True),
        assume_consecutive_token_ids =
            global_parameter("assume_consecutive_token_ids", True),
        add_permissions_descriptor =
            global_parameter("add_permissions_descriptor", False),
        lazy_entry_points = global_parameter("lazy_entry_points", False),
        lazy_entry_points_multiple = global_parameter("lazy_entry_points_multiple", False),
    )

## ## Standard “main”
##
## This specific main uses the relative new feature of non-default tests
## for the browser version.
if "templates" not in __name__:
    add_test(environment_config())
    if not global_parameter("only_environment_test", False):
        add_test(FA2_config(debug_mode = True), is_default = not sp.in_browser)
        add_test(FA2_config(single_asset = True), is_default = not sp.in_browser)
        add_test(FA2_config(non_fungible = True, add_mutez_transfer = True),
                 is_default = not sp.in_browser)
        add_test(FA2_config(readable = False), is_default = not sp.in_browser)
        add_test(FA2_config(force_layouts = False),
                 is_default = not sp.in_browser)
        # add_test(FA2_config(debug_mode = True, support_operator = False),
        #          is_default = not sp.in_browser)
        add_test(FA2_config(assume_consecutive_token_ids = False)
                 , is_default = not sp.in_browser)
        add_test(FA2_config(lazy_entry_points = True)
                 , is_default = not sp.in_browser)
        add_test(FA2_config(lazy_entry_points_multiple = True)
                 , is_default = not sp.in_browser)
